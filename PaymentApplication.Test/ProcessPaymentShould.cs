﻿using System;
using System.Collections.Generic;
using Xunit;
using static PaymentApplication.Program;

namespace PaymentApplication.Test
{
    public class ProcessPaymentShould
    {
        [Fact]
        public void FailValidationForCreditCardNumber()
        {
            var amount = 20m;
            var creditCardNumber = "234567890123456";
            var cardHolder = "Me";
            var expirationDate = DateTime.Today.AddDays(-1);
            var securityCode = 123;

            var creditCard = new CreditCard(creditCardNumber, cardHolder, expirationDate, securityCode, amount);

            var exception = Assert.Throws<InvalidCreditCardException>(() => ProcessPayment(creditCard));
            List<string> messages = new List<string>
            {
                "CreditCardNumber should contains 16 digits",
                "Expiration date cannot be before today."
            };
            Assert.Equal(messages, exception.ErrorMessages());

        }

        [Fact]
        public void ProcessPaymentUsingExpensiveAmount()
        {
            var amount = 58m;
            var creditCardNumber = "1234567890123456";
            var cardHolder = "Me";
            var expirationDate = DateTime.Today;
            var securityCode = 123;

            var creditCard = new CreditCard(creditCardNumber, cardHolder, expirationDate, securityCode, amount);

            ProcessPayment(creditCard);
        }
    }
}
